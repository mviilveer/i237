# I237 Course Application

# How to install

Currently to test this code you have to clone the repository

```bash
git clone git@bitbucket.org:mviilveer/i237.git
cd i237
```

Export ARDUINO device path environment variable.
Example:
```bash
export ARDUINO=/dev/ttyACM0
```

After that you can `make` the project.

```bash
make clean
make
make install
```

# License

```Text
Copyright (C) 2016 Mihkel Viilveer <viilveer@hotmail.com>

This file is part of I237 Course Application.

I237 Course Application is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

I237 Course Application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with I237 Course Application.  If not, see <http://www.gnu.org/licenses/>.
```
