/*
 *  Copyright (C) 2016 Mihkel Viilveer <viilveer@hotmail.com>
 *
 *  This file is part of I237 Course Application.
 *
 *  I237 Course Application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  I237 Course Application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with I237 Course Application.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define STUD_NAME "Mihkel Viilveer"
#define VER_FW "Version: " GIT_DESCR " built on: " __DATE__ " " __TIME__ "\r\n"
#define VER_LIBC_GCC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"
#define INPUT_MONTH_LETTER "Enter Month name first letter >"
#define UPTIME "Uptime: %lu s"

#define ACCESS_DENIED "Access denied"
#define NEW_CARD_ADDED "New card added"
#define NO_CARD_NEARBY "Unable to find card."
#define CARD_DELETED "Deleted card for user: "
#define CARD_DELETE_FAILED "Did not find card for: "
#define MEM_OPERATION_FAIL "Memory operation failed "

#define NAME_MONTH_COUNT 6

#define HELP_CMD "help"
#define HELP_HELP "Get help"
#define VER_CMD "version"
#define VER_HELP "Print FW version"
#define ASCII_CMD "ascii"
#define ASCII_HELP "Print ASCII table"
#define CARD_READ_CMD "read"
#define CARD_READ_HELP "Read card information"
#define CARD_ADD_CMD "add"
#define CARD_ADD_HELP "Add new card information. Card must be near reader when using the command. Usage add <string>"
#define CARD_LIST_CMD "print"
#define CARD_LIST_HELP "List all entered cards"
#define CARD_DELETE_CMD "remove"
#define CARD_DELETE_HELP "Delete single card by person name. Usage remove <string>"
#define DEVICE_MEM_USAGE_CMD "mem"
#define DEVICE_MEM_USAGE_HELP "View device memory usage"
#define MONTH_CMD "month"
#define MONTH_HELP "Find matching month from lookup list. Usage: month <string>"

#define CLI_HELP_MSG "Implemented commands"
#define CLI_NOT_IMPLEMENTED "Command not implemented.\r\n Use <help> to get help."
#define CLI_TYPE_HELP "Use <help> to get help.\r\n"
#define CLI_MISSING_ARGUMENTS "To few or too many arguments for this command\r\n Use <help>"

extern PGM_P const monthNames[] PROGMEM;

extern const char help_cmd[] PROGMEM;
extern const char help_help[] PROGMEM;
extern const char ver_cmd[] PROGMEM;
extern const char ver_help[] PROGMEM;
extern const char ascii_cmd[] PROGMEM;
extern const char ascii_help[] PROGMEM;
extern const char card_read_cmd[] PROGMEM;
extern const char card_read_help[] PROGMEM;
extern const char card_add_cmd[] PROGMEM;
extern const char card_add_help[] PROGMEM;
extern const char card_list_cmd[] PROGMEM;
extern const char card_list_help[] PROGMEM;
extern const char card_delete_cmd[] PROGMEM;
extern const char card_delete_help[] PROGMEM;
extern const char device_mem_usage_cmd[] PROGMEM;
extern const char device_mem_usage_help[] PROGMEM;
extern const char month_cmd[] PROGMEM;
extern const char month_help[] PROGMEM;

// _HMI_MSG_H_
#endif
