#include <avr/pgmspace.h>
#include "hmi_msg.h"


static const char january[] PROGMEM = "January";
static const char february[] PROGMEM = "February";
static const char march[] PROGMEM = "March";
static const char april[] PROGMEM = "April";
static const char may[] PROGMEM = "May";
static const char june[] PROGMEM = "June";

PGM_P const monthNames[] PROGMEM = {
    january,
    february,
    march,
    april,
    may,
    june
};

const char help_cmd[] PROGMEM = HELP_CMD;
const char help_help[] PROGMEM = HELP_HELP;
const char ver_cmd[] PROGMEM = VER_CMD;
const char ver_help[] PROGMEM = VER_HELP;
const char ascii_cmd[] PROGMEM = ASCII_CMD;
const char ascii_help[] PROGMEM = ASCII_HELP;
const char card_read_cmd[] PROGMEM = CARD_READ_CMD;
const char card_read_help[] PROGMEM = CARD_READ_HELP;
const char card_add_cmd[] PROGMEM = CARD_ADD_CMD;
const char card_add_help[] PROGMEM = CARD_ADD_HELP;
const char card_list_cmd[] PROGMEM = CARD_LIST_CMD;
const char card_list_help[] PROGMEM = CARD_LIST_HELP;
const char card_delete_cmd[] PROGMEM = CARD_DELETE_CMD;
const char card_delete_help[] PROGMEM = CARD_DELETE_HELP;
const char device_mem_usage_cmd[] PROGMEM = DEVICE_MEM_USAGE_CMD;
const char device_mem_usage_help[] PROGMEM = DEVICE_MEM_USAGE_HELP;
const char month_cmd[] PROGMEM = MONTH_CMD;
const char month_help[] PROGMEM = MONTH_HELP;
