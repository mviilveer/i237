/*
 *  Copyright (C) 2016 Mihkel Viilveer <viilveer@hotmail.com>
 *
 *  This file is part of I237 Course Application.
 *
 *  I237 Course Application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  I237 Course Application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with I237 Course Application.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _HMI_MICRORL_H_
#define _HMI_MICRORL_H_

#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))
#define UART_STATUS_MASK    0x00FF

typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

void cli_print(const char * str);
char cli_get_char(void);
int cli_execute(int argc, const char *const *argv);

// _HMI_MICRORL_H_
#endif