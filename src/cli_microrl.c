#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>

#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "hmi_msg.h"
#include "rc522.h"
#include "print_helper.h"
#include "cli_microrl.h"



void cli_print_help(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_rfid_read(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_list(const char *const *argv);
void cli_rfid_delete(const char *const *argv);
void cli_mem_stat(const char *const *argv);
void cli_handle_month(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);


const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {card_read_cmd, card_read_help, cli_rfid_read, 0},
    {card_add_cmd, card_add_help, cli_rfid_add, 1},
    {card_list_cmd, card_list_help, cli_rfid_list, 0},
    {card_delete_cmd, card_delete_help, cli_rfid_delete, 1},
    {device_mem_usage_cmd, device_mem_usage_help, cli_mem_stat, 0},
    {month_cmd, month_help, cli_handle_month, 1}
};


char cli_get_char(void)
{
    if (uart0_peek() != UART_NO_DATA) {
        return uart0_getc() & UART_STATUS_MASK;
    } else {
        return 0x00;
    }
}


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR(CLI_HELP_MSG "\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}


void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR(VER_FW));
    uart0_puts_p(PSTR(VER_LIBC_GCC));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("\r\n"));
    /* Print ascii table */
    print_ascii_tbl(uart0_puts);
    unsigned char ascii[128] = {0};

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human(uart0_puts, ascii, sizeof(ascii));
}


void cli_handle_month(const char *const *argv)
{
    uart0_puts_p(PSTR("\r\n"));
    lcd_clr(0x40, 26);
    lcd_goto(0x40);

    for (int i = 0; i < NAME_MONTH_COUNT; i++) {
        if (!strncmp_P(argv[1], (PGM_P)pgm_read_word(&monthNames[i]),
                       strlen(argv[1]))) {
            lcd_puts_P((PGM_P)pgm_read_word(&monthNames[i]));
            lcd_putc(' ');
            uart0_puts_p((PGM_P)pgm_read_word(&monthNames[i]));
            uart0_puts_p(PSTR("\r\n"));
        }
    }
}

void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;

    if (PICC_IsNewCardPresent()) {
        PICC_ReadCardSerial(uid_ptr);
        char write_buffer[16];

        for (byte i = 0; i < uid.size; i++) {
            sprintf(write_buffer, "%02X", uid.uidByte[i]);
            uart0_puts(write_buffer);
        }

        uart0_puts_p(PSTR("\r\n"));
    } else {
        uart0_puts_p((PSTR(NO_CARD_NEARBY "\r\n")));
    }
}

void cli_rfid_add(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;

    if (PICC_IsNewCardPresent()) {
        PICC_ReadCardSerial(uid_ptr);
        card_t *newCard = create_card(argv[1], uid.uidByte, uid.size);
        card_t *existing_card = find_card_by_code(newCard->code);

        if (existing_card) {
            print_single_card(existing_card);
            uart0_puts_p(PSTR("\r\n"));
        } else {
            add_card(newCard);

            uart0_puts_p(PSTR(NEW_CARD_ADDED "\r\n"));
        }
    } else {
        uart0_puts_p((PSTR(NO_CARD_NEARBY "\r\n")));
    }
}

void cli_rfid_list(const char *const *argv)
{
    (void) argv;
    print_cards();
}

void cli_rfid_delete(const char *const *argv)
{
    (void) argv;
    remove_card(argv[1]);
}

/**
 * This is a debug function, moving strings to hmi_msg is not needed
 * @param argv
 */
void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    char mem_write_buffer[512];
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    uart0_puts_p(PSTR("Heap statistics\n\r"));
    sprintf_P(mem_write_buffer, PSTR("Used: %d\n\r"), getMemoryUsed());
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Free: %d\n\r"), getFreeMemory());
    uart0_puts(mem_write_buffer);
    uart0_puts_p(PSTR("Space between stack and heap:\n\r"));
    sprintf_P(mem_write_buffer, PSTR("Current %d\n\r"), space);
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Previous %d\n\r"), prev_space);
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Change %d\n\r"), space - prev_space);
    uart0_puts(mem_write_buffer);
    uart0_puts_p(PSTR("Freelist\n\r"));
    sprintf_P(mem_write_buffer, PSTR("Freelist size: %d\n\r"), getFreeListSize());
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Blocks in freelist: %d\n\r"),
              getNumberOfBlocksInFreeList());
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Largest block in freelist: %d\n\r"),
              getLargestBlockInFreeList());
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Largest freelist block: %d\n\r"),
              getLargestAvailableMemoryBlock());
    uart0_puts(mem_write_buffer);
    sprintf_P(mem_write_buffer, PSTR("Largest allocable block: %d\n\r"),
              getLargestNonFreeListBlock());
    uart0_puts(mem_write_buffer);
    prev_space = space;
}


void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR(CLI_NOT_IMPLEMENTED "\r\n"));
}


void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR(CLI_MISSING_ARGUMENTS "\r\n"));
}


int cli_execute(int argc, const char *const *argv)
{
    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function pointer,
            // cross fingers and hope that function handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}
