#include <stdio.h>
#include "print_helper.h"

void print_ascii_tbl (void (*putcharacter)(const char*))
{
    char toPrint[1];

    for (char c = ' '; c <= '~'; c++) {
        sprintf(toPrint, "%c", c);
        putcharacter(toPrint);
    }

    putcharacter("\r\n");
}

void print_for_human(void (*putcharacter)(const char*),
                     const unsigned char *array, const size_t len)
{
    char toPrint[1];

    for (unsigned i = 0; i < len; i++) {
        if (array[i] >= ' ' && array[i] <= '~') {
            sprintf(toPrint, "%c", array[i]);
        } else {
            sprintf(toPrint, "\"0x%02x\"", array[i]);
        }

        putcharacter(toPrint);
    }

    putcharacter("\r\n");
}