#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"

#include "print_helper.h"
#include "hmi_msg.h"
#include "rc522.h"
#include "cli_microrl.h"

#define BAUD 9600

#define LED_INIT DDRA |= _BV(DDA3)
#define DOOR_INIT DDRA |= _BV(DDA1)
#define DOOR_OPEN PORTA |= _BV(PORTA1)
#define DOOR_CLOSE PORTA &= ~_BV(PORTA1)

volatile uint32_t time;

static inline void init_system_clock(void)
{
    TCCR5A = 0; // Clear control register A
    TCCR5B = 0; // Clear control register B
    TCCR5B |= _BV(WGM52) | _BV(CS52); // CTC and fCPU/256
    OCR5A = 62549; // 1 s
    TIMSK5 |= _BV(OCIE5A); // Output Compare A Match Interrupt Enable
}

static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}

static inline void hw_init()
{
    /* Set pin 3 of PORTA for output */
    LED_INIT;
    DOOR_INIT;
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    init_system_clock();
    init_rfid_reader();
    sei();
    lcd_init();
    lcd_clrscr();
}

static inline void print_info()
{
    uart3_puts_p(PSTR(VER_FW));
    uart3_puts_p(PSTR(VER_LIBC_GCC));
    uart0_puts_p(PSTR(STUD_NAME "\r\n"));
    lcd_puts_P(PSTR(STUD_NAME));
    uart0_puts_p(PSTR(CLI_TYPE_HELP));
}


static inline void heartbeat()
{
    static uint32_t last_time;
    uint32_t cur_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        cur_time = time;
    }

    if ((last_time - time) > 0) {
        // Toggle LED
        PORTA ^= _BV(PORT3);
        char uptime[32];
        sprintf_P(uptime, PSTR(UPTIME "\r\n"), cur_time);
        uart3_puts(uptime);
    }

    last_time = cur_time;
}

static inline void handle_door()
{
    Uid uid;
    uint32_t time_cur;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        time_cur = time;
    }
    static uint32_t message_start;
    static uint32_t door_open_start;

    if (PICC_IsNewCardPresent()) {
        lcd_clr(0x40, 26);
        PICC_ReadCardSerial(&uid);
        card_t *found_card = find_card_by_code(uid.uidByte);

        if (found_card != NULL) {
            lcd_goto(0x40);
            lcd_puts(found_card->name);
            DOOR_OPEN;
        } else {
            DOOR_CLOSE;
            lcd_goto(0x40);
            lcd_puts_P(PSTR(ACCESS_DENIED));
        }

        door_open_start = time_cur;
        message_start = time_cur;
    }

    if ((message_start + 5) < time_cur) {
        message_start = time_cur + 5;
        lcd_clr(0x40, 26);
        lcd_goto(0x40);
    }

    if ((door_open_start + 2) < time_cur) {
        DOOR_CLOSE;
    }
}


void main (void)
{
    hw_init();
    print_info();
    //Create microrl object and pointer on it
    microrl_t rl;
    microrl_t *prl = &rl;
    microrl_init(prl, uart0_puts);
    microrl_set_execute_callback(prl, cli_execute);

    while (1) {
        heartbeat();
        handle_door();
        microrl_insert_char(prl, cli_get_char());
    }
}

/* System clock ISR */
ISR(TIMER5_COMPA_vect)
{
    time++;
}
