/*
 *  Copyright (C) 2016 Mihkel Viilveer <viilveer@hotmail.com>
 *
 *  This file is part of I237 Course Application.
 *
 *  I237 Course Application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  I237 Course Application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with I237 Course Application.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HMI_RC522_H_
#define _HMI_RC522_H_

typedef struct card
{
    char *name;
    uint8_t *code;
    uint8_t code_size;
    struct card *next;
} card_t;

extern card_t *firstCard;

extern card_t *create_card(const char *name, uint8_t *code, uint8_t size);
extern void add_card(card_t *new_card);
extern card_t* find_card_by_code(const uint8_t *code);
extern void remove_card(const char *name);
extern void print_cards();
extern void print_single_card(const card_t *card);

// _HMI_RC522_H_
#endif