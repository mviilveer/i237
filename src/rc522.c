#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>

#include "hmi_msg.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "rc522.h"

card_t *first_card;

/* That is one possible option to create first list item. Useful when handling multiple lists in same program */
card_t *create_card(const char *name, uint8_t *code, uint8_t size)
{
    card_t *new_card = NULL;
    new_card = malloc(sizeof(card_t));

    if (new_card == NULL) {
        uart0_puts_p(PSTR(MEM_OPERATION_FAIL "\r\n"));
        exit(1);
    }

    new_card->name = malloc(strlen(name) + 1);

    if (new_card->name == NULL) {
        uart0_puts_p(PSTR(MEM_OPERATION_FAIL "\r\n"));
        exit(1);
    }

    strcpy(new_card->name, name);
    new_card->code_size = size;
    new_card->code = malloc(size);

    if (new_card->code == NULL) {
        uart0_puts_p(PSTR(MEM_OPERATION_FAIL "\r\n"));
        exit(1);
    }

    memcpy(new_card->code, code, size);
    new_card->next = NULL;
    return new_card;
}

void add_card(card_t *new_card)
{
    if (first_card == NULL) {
        first_card = new_card;
    } else {
        card_t *current = first_card;

        while (current->next != NULL) {
            current = current->next;
        }

        current->next = new_card;
    }
}

card_t* find_card_by_code(const uint8_t *code)
{
    if (first_card != NULL) {
        card_t *tmp_card = first_card;

        while (tmp_card != NULL) {
            if (*code == *tmp_card->code) {
                return tmp_card;
            }

            tmp_card = tmp_card->next;
        }
    }

    return NULL;
}

void remove_card(const char *name)
{
    card_t *current = first_card;
    card_t *prev = NULL;

    while (current != NULL) {
        if (strcmp(current->name, name) == 0) {
            if (prev == NULL) {
                // Special condition when first item is removed
                // No need to change next value. Just point head to next item
                first_card = current->next;
            } else {
                prev->next = current->next;
            }

            free(current->name);
            free(current->code);
            free(current);
            uart0_puts_p(PSTR(CARD_DELETED));
            uart0_puts(name);
            uart0_puts_p(PSTR("\r\n"));
            return;
        }

        prev = current;
        current = current->next;
    }

    uart0_puts_p(PSTR(CARD_DELETE_FAILED));
    uart0_puts(name);
    uart0_puts_p(PSTR("\r\n"));
}
void print_single_card(const card_t *card)
{
    char write_buffer[128];

    for (uint8_t i = 0; i < card->code_size; i++) {
        sprintf(write_buffer, "%02X", card->code[i]);
        uart0_puts(write_buffer);
    }

    sprintf(write_buffer, " : %s", card->name);
    uart0_puts(write_buffer);
}


void print_cards()
{
    card_t *current = first_card;
    uart0_puts("Inserted cards:\r\n");

    while (current != NULL) {
        print_single_card(current);
        uart0_puts_p(PSTR("\r\n"));
        current = current->next;
    }
}
